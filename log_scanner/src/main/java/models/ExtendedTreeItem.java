package models;

import javafx.scene.control.TreeItem;

import java.io.File;
import java.nio.file.Path;

public class ExtendedTreeItem extends TreeItem<String> {

    private final File file;

    public ExtendedTreeItem(File file) {
        super(file.toString());
        this.file = file;

        String fullPath = file.getAbsolutePath();
        if (!fullPath.endsWith(File.separator)) {
            String value = file.toString();
            int indexOf = value.lastIndexOf(File.separator);
            if (indexOf > 0) {
                this.setValue(value.substring(indexOf + 1));
            } else {
                this.setValue(value);
            }
        }
    }

    public Path getPath() {
        return file.toPath();
    }

    @Override
    public boolean isLeaf(){
       return this.file.isFile();
    }
}
