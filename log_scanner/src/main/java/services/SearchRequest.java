package services;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;

public class SearchRequest {

    private Path path;
    private byte[] text;
    private Predicate<Path> extensionFilter;

    public SearchRequest(Path path, String extension, byte[] text) {
        this.path = path;
        this.extensionFilter = extension.equals("*") ?
                file -> true :
                file -> file.toString().endsWith("." + extension);
        this.text = text;
    }

    public SearchRequest() {
        this.path = Paths.get(System.getProperty("user.dir"));
        this.extensionFilter = file -> true;
    }

    public Path getPath() {
        return path;
    }

    public Predicate<Path> getExtensionFilter() {
        return extensionFilter;
    }

    public byte[] getText() {
        return text;
    }

    public void setText(byte[] text) {
        this.text = text;
    }
}
