package services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileViewService extends Service<List<String>> {

    private Path path;

    private Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    @Override
    protected Task<List<String>> createTask() {
        final Path _path = getPath();
        return new Task<List<String>>() {
            @Override
            protected List<String> call() throws Exception {
                List<String> lines = new ArrayList<>();
                try {
                    lines = Files.readAllLines(_path,
                            Charset.defaultCharset());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Thread.sleep(5000);

                return lines;
            }
        };
    }
}
