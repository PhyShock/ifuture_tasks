package services;

import java.nio.file.Path;
import java.util.concurrent.Callable;

public class TextFilter implements Callable<Integer> {

    private Path file;
    private String text;

    public TextFilter(Path file, String text) {
        this.file = file;
        this.text = text;
    }

    public static boolean textFilter(Path path) {
        return true;
    }

    @Override
    public Integer call() throws Exception {

        Thread.sleep(3000);
        System.out.println(Thread.currentThread().getId());

        return 3;
    }
}
