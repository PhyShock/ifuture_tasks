package services;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import models.ExtendedTreeItem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileCollectorService extends Service<TreeItem<String>> {

    private SearchRequest searchRequest;
    private ExecutorService executorService;

    public FileCollectorService() {
        this.searchRequest = new SearchRequest();
        this.executorService = Executors.newCachedThreadPool();
        setExecutor(executorService);
    }

    @Override
    protected Task<TreeItem<String>> createTask() {
        return new Task<TreeItem<String>>() {
            @Override
            protected TreeItem<String> call() throws Exception {

                TreeItem<String> root = new ExtendedTreeItem(searchRequest.getPath().toFile());
                listFiles(root);
                root.setExpanded(true);

                return root;
            }
        };
    }

    public void setSearchRequest(SearchRequest searchRequest) {
        this.searchRequest = searchRequest;
    }

    private void listFiles(TreeItem<String> root) {

        try (Stream<Path> pathStream = Files.walk(searchRequest.getPath())) {

            List<Path> filtered = pathStream
                    .parallel()
                    .filter(searchRequest.getExtensionFilter())
                    .filter(Files::isRegularFile).collect(Collectors.toList());
            
            //String type just for tests!
            Map<Path, Future<Integer>> results = new HashMap<>();
            filtered.forEach(path -> results.put(path, executorService.submit(new TextFilter(path, Arrays.toString(searchRequest.getText())))));

            Map<Path, TreeItem<String>> map = new HashMap<>();
            map.put(searchRequest.getPath(), root);

            filtered.stream()
                    .filter(path -> {
                        try {
                            return !results.get(path).get().equals(-1);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            return false;
                        }
                    })
                    .forEach(path -> buildTreeView(map, root, path));


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private TreeItem<String> buildTreeView(Map<Path, TreeItem<String>> files, TreeItem<String> root, Path file) {

        ExtendedTreeItem result = (ExtendedTreeItem) files.get(file);

        if (result == null) {

            result = new ExtendedTreeItem(file.toFile());
            files.put(file, result);

            if (file.getNameCount() - searchRequest.getPath().getNameCount() > 1) {

                TreeItem<String> parent = buildTreeView(files, root, file.subpath(0, file.getNameCount() - 1));
                parent.getChildren().add(result);

            } else root.getChildren().add(result);

        }

        return result;
    }

}
