package services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.util.Pair;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;

public class FileViewTask extends Task<Pair<ObservableList<String>, Long>> {
    private final Path path;
    private final long position;
    private final int direction;

    private final int chunkSize = 400;

    public FileViewTask(Path path, long position, int direction) {
        this.path = path;
        this.position = position;
        this.direction = direction;
    }

    @Override
    protected Pair<ObservableList<String>, Long> call() throws Exception {
        //For small files just readAll pls........
        try (SeekableByteChannel seekableByteChannel = Files.newByteChannel(path)) {

            setStartPosition(seekableByteChannel);

            ByteBuffer buffer;

            // For small movements near the start of the file
            if (direction > 0)
                buffer = position > chunkSize ? ByteBuffer.allocate(chunkSize) : ByteBuffer.allocate((int) position);
            else buffer = ByteBuffer.allocate(chunkSize);


            String encoding = System.getProperty("file.encoding");

            seekableByteChannel.read(buffer);
            buffer.rewind();

            CharBuffer charBuffer = Charset.forName(encoding).decode(buffer);

            int positionInChunk = lastLinePosition(charBuffer);
            boolean endOfFile = isEnd(charBuffer, positionInChunk);

            long newPosition = seekableByteChannel.position();

            //Case for the small files ( they fits the first chunk ) ( Correcting inChannel position
            if (newPosition <= chunkSize)
                seekableByteChannel.position(positionInChunk);
            else seekableByteChannel.position(newPosition - (chunkSize - positionInChunk));

            long currentPosition = seekableByteChannel.position();

            charBuffer = charBuffer.subSequence(0, positionInChunk);

            ObservableList<String> lines = FXCollections.observableArrayList(Arrays
                    .stream(String.valueOf(charBuffer).split("\n"))
                    .collect(Collectors.toList()));

            return new Pair<>(lines, endOfFile ? (long) -1 : currentPosition);
        }
    }


    //scroll up in 0 pos, will work as scroll down!! ( feature )
    private void setStartPosition(SeekableByteChannel seekableByteChannel) throws IOException {

        if (direction < 0)
            seekableByteChannel.position(position);
        else if (position > chunkSize)
            seekableByteChannel.position(position - chunkSize);
        else
            seekableByteChannel.position(0);

    }

    //outOfBound care, for the else if case
    private int lastLinePosition(CharBuffer charBuffer) {

        for (int i = charBuffer.length() - 1; i >= 0; i--) {
            char symbol = charBuffer.charAt(i);
            if (symbol == '\n')
                return i + 1;
            else if (symbol != '\u0000' && i != charBuffer.length() - 1 && charBuffer.charAt(i + 1) == '\u0000')
                return i + 1;
        }

        return -1;
    }

    private boolean isEnd(CharBuffer charBuffer, int position) {
        return (charBuffer.charAt(position) == '\u0000');
    }

}
