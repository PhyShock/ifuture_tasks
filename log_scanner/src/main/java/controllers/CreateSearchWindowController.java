package controllers;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import services.SearchRequest;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateSearchWindowController {
    public TextField pathField;
    public TextField extensionField;
    public TextArea textArea;
    public Button searchButton;
    public Button selectPathButton;

    private SearchRequest searchRequest;
    private DirectoryChooser directoryChooser;
    private Path path;
    private Stage stage;

    public CreateSearchWindowController(Stage stage) {
        this.stage = stage;
    }

    public void initialize() {

        directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(Paths.get(System.getProperty("user.dir")).toFile());

        selectPathButton.setOnAction(event -> {
            File selectedDirectory = directoryChooser.showDialog(stage);
            path = selectedDirectory.toPath();
            pathField.setText(path.toString());
        });

        searchButton.setOnAction(event -> {
            if (!isFieldsAreEmpty()) {
                searchRequest = new SearchRequest(
                        path,
                        extensionField.getText(),
                        textArea.getText().getBytes()
                );


                closeStage();
            }
        });
    }

    public SearchRequest getSearchRequest() {
        return searchRequest;
    }

    private boolean isFieldsAreEmpty() {

        StringBuilder errors = new StringBuilder();

        if (pathField.getText().trim().isEmpty()) {
            errors.append("- Please enter a path to the search directory\n");
        }
        if (extensionField.getText().trim().isEmpty()) {
            errors.append("- Please enter a file extension\n");
        }
        if (textArea.getText().getBytes().length == 0) {
            errors.append("- Please enter the text\n");
        }

        if (errors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Required Fields Empty");
            alert.setContentText(errors.toString());

            alert.showAndWait();
            return true;
        }

        return false;
    }

    private void closeStage() {
        if (stage != null) {
            stage.close();
        }
    }

}
