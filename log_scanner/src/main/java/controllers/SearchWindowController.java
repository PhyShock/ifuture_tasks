package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import models.ExtendedTreeItem;
import services.FileCollectorService;
import services.FileViewTask;
import services.SearchRequest;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class SearchWindowController {

    public TreeView<String> tree_view;
    public TabPane tab_pane;
    public MenuItem newSearchButton;
    public MenuBar menuBar;
    public StackPane stackPane;
    public ProgressIndicator progressIndicator;

    private Map<String, Long> openFiles;
    private FileCollectorService fileCollectorService;

    //Very bad practise
    private int lastDirection = -1;

    public void initialize() {
        fileCollectorService = new FileCollectorService();
        openFiles = new HashMap<>();

        progressIndicator.setMaxSize(50, 50);

        fileCollectorService.setOnRunning(event -> {
            tree_view.setVisible(false);
            progressIndicator.setVisible(true);

        });

        fileCollectorService.setOnSucceeded(event -> {
            progressIndicator.setVisible(false);
            tree_view.setVisible(true);
            tree_view.setRoot(fileCollectorService.getValue());
        });

        fileCollectorService.start();

        tab_pane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);

        tree_view.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2)
                openFileInTab((ExtendedTreeItem) tree_view.getSelectionModel().getSelectedItem());
        });

    }

    private void openFileInTab(ExtendedTreeItem file) {

        if (file.isLeaf()) {

            if (fileNotOpenYet(file.getValue())) {
                Tab tab = new Tab();
                tab.setText(file.getValue());
                tab_pane.getTabs().add(tab);
                tab_pane.getSelectionModel().select(tab);

                ListView<String> listView = new ListView<>(FXCollections.observableArrayList());
                readFilePart(file.getPath(), (long) 0, listView.getItems(), -1);
                listView.fixedCellSizeProperty().set(24);
                tab.setContent(listView);

//                listView.addEventFilter(ScrollEvent.SCROLL, event -> {
//
//                    String fileName = file.getPath().getFileName().toString();
//
//                    int direction = (int) event.getDeltaY();
//
//
//                    if (openFiles.get(fileName) == -1)
//                        return;
//
//                    ObservableList<String> lines = listView.getItems();
//                    int displayingLength = lines.stream().mapToInt(String::length).sum() + lines.size();
//                    System.out.println(displayingLength);
//                    long currentPosition = isDirectionChanged(direction) ?
//                            correctPosition(direction, openFiles.get(fileName), displayingLength) :
//                            openFiles.get(fileName);
//
//                    readFilePart(file.getPath(), currentPosition, lines, direction);
//                    tab.setContent(listView);
//
//                    lastDirection = direction;
//
//                });

                listView.setOnScroll(event -> {

                    //TODO
                    // 1. Fix file reading! ( bad scrolling, can't find position after up-down-up, bad row calculation ).
                    // 2. Now it is better to start implementing text finder.

                    String fileName = file.getPath().getFileName().toString();

                    int direction = (int) event.getDeltaY();


                    if (openFiles.get(fileName) == -1)
                        return;

                    ObservableList<String> lines = listView.getItems();
                    int displayingLength = lines.stream().mapToInt(String::length).sum() + lines.size();
                    System.out.println(displayingLength);
                    long currentPosition = isDirectionChanged(direction) ?
                            correctPosition(direction, openFiles.get(fileName), displayingLength) :
                            openFiles.get(fileName);

                    readFilePart(file.getPath(), currentPosition, lines, direction);
                    tab.setContent(listView);

                    lastDirection = direction;

                });

            } else
                tab_pane.getSelectionModel().select(tab_pane.getTabs().stream()
                        .filter(tab -> tab.getText().equals(file.getValue())).findFirst().get());

        }
    }

    private boolean isDirectionChanged(int newDirection) {
        return Integer.signum(lastDirection) != Integer.signum(newDirection);

    }

    private long correctPosition(int direction, Long lastPosition, int displayingLength) {

        if (direction > 0)
            return lastPosition - displayingLength;
        else
            return lastPosition + displayingLength;

    }

    private void readFilePart(Path path, long position, ObservableList<String> lines, int direction) {

        FileViewTask fileViewTask = new FileViewTask(path, position, direction);
        fileViewTask.setOnSucceeded(event -> {
            Pair<ObservableList<String>, Long> result = fileViewTask.getValue();

            int listSize = lines.size();

            if (listSize > 0) {
                if (direction < 0) {
                    lines.remove(0, 2);
                    lines.addAll(result.getKey());
                } else {
                    lines.remove(listSize - 3, listSize - 1);
                    lines.addAll(0, result.getKey());
                }
            } else lines.addAll(result.getKey());

            openFiles.put(path.getFileName().toString(), result.getValue());
        });
        new Thread(fileViewTask).start();
    }

    private boolean fileNotOpenYet(String fileName) {

        return tab_pane.getTabs().stream()
                .noneMatch(tab -> tab.getText().equals(fileName));

    }

    public void startNewSearch(ActionEvent actionEvent) {

        fileCollectorService.setSearchRequest(showPopupWindow());
        fileCollectorService.restart();
    }

    private SearchRequest showPopupWindow() {

        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getClassLoader().getResource("create_search_window.fxml"));

        Stage popupStage = new Stage();
        CreateSearchWindowController createSearchWindowController = new CreateSearchWindowController(popupStage);
        loader.setController(createSearchWindowController);
        Parent layout;
        try {
            layout = loader.load();
            Scene scene = new Scene(layout);
            popupStage.initOwner(getPrimaryStage());
            popupStage.initModality(Modality.WINDOW_MODAL);
            popupStage.setScene(scene);
            popupStage.setResizable(false);
            popupStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return createSearchWindowController.getSearchRequest();
    }

    private Stage getPrimaryStage() {
        return (Stage) menuBar.getScene().getWindow();
    }


}

